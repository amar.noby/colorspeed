using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChipBehavior : MonoBehaviour
{
    public static float speed = 1;
    public static float speedIncrease = 0.05f;
    public bool isMoving = true;
    SceneSwitcher sceneSwitcher;
    private void OnEnable()
    {
        isMoving = true;
        sceneSwitcher = GameObject.Find("GameManager").GetComponent<SceneSwitcher>();
    }
    // Update is called once per frame
    void Update()
    {
        if (isMoving)
        {
            transform.Translate(Vector2.up * speed * Time.deltaTime);
        }
        if (transform.position.y > Camera.main.orthographicSize)
        {
            sceneSwitcher.GoToLeaderboard();
        }
    }
    
}
