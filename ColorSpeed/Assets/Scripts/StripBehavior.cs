using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StripBehavior : MonoBehaviour
{
    public GameManager GameManager;
    public SceneSwitcher SceneSwitcher;
    AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        collision.gameObject.SetActive(false);
        collision.gameObject.GetComponent<ChipBehavior>().isMoving = true;
        if (collision.CompareTag(gameObject.tag))
        {
            ChipBehavior.speed += ChipBehavior.speedIncrease;
            Score.score += 5;
            
        }
        else
        {
            SceneSwitcher.GoToLeaderboard();
            audioSource.Play();
        }
    }
}
