using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
    public void LoadGame()
    {
        StartCoroutine(LoadGameCoroutine());
    }
    public void LoadSettings()
    {
        StartCoroutine(LoadSettingsCoroutine());
    }
    public void GoToLeaderboard()
    {
        StartCoroutine(GoToLeaderboardWait());
    }
    public void LoadWithoutLoading()
    {
        SceneManager.LoadScene("MainGame");
    }

    IEnumerator LoadGameCoroutine()
    {
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("MainGame");
    }
    IEnumerator LoadSettingsCoroutine()
    {
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("Settings");
    }
    IEnumerator GoToLeaderboardWait()
    {
        yield return new WaitForSeconds(0.1f);
        SceneManager.LoadScene("Leaderboard");
    }
}
