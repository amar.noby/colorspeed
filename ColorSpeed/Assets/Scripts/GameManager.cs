using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Transform SpawnPosition;
    public GameObject YellowChip;
    public GameObject WhiteChip;
    public const int numOfChips = 30;
    public GameObject[,] Chips = new GameObject[2, numOfChips];
    private int totalChipsPlaced = 0;
    public GameObject TopChip;
    public float lateralSpeed;

    // Start is called before the first frame update
    void Start()
    {
        CreateChipsPool();
        PlaceChips();
        GetTopChip();
        ChipBehavior.speed = 1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void CreateChipsPool()
    {
        for (int i = 0; i < numOfChips; i++)
        {
            Chips[0, i] = Instantiate(YellowChip);
            Chips[0, i].SetActive(false);
        }
        for (int i = 0; i < numOfChips; i++)
        {
            Chips[1, i] = Instantiate(WhiteChip);
            Chips[1, i].SetActive(false);
        }
    }

    public void PlaceChips()
    {
        int randomChip = Random.Range(0, 2);
        for (int i = 0; i < numOfChips; i++)
        {
            if (!Chips[randomChip, i].activeInHierarchy)
            {
                Chips[randomChip, i].SetActive(true);
                Chips[randomChip, i].transform.position = SpawnPosition.position;
                Chips[randomChip, i].transform.rotation = SpawnPosition.rotation;
                
                SpriteRenderer sr = Chips[randomChip, i].GetComponent<SpriteRenderer>();
                sr.sortingOrder = -totalChipsPlaced;
                totalChipsPlaced++;
                break;
            }
        }
    }
    public void GetTopChip()
    {
        GameObject topChip = null;
        float maxY = float.MinValue;
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < numOfChips; j++)
            {
                if (Chips[i, j].activeInHierarchy && Chips[i, j].transform.position.y > maxY && Chips[i,j].transform.position.x == 0)
                {
                    topChip = Chips[i, j];
                    maxY = Chips[i, j].transform.position.y;
                }
            }
        }
        TopChip = topChip;
    }
    public void MoveChipToStrip(bool isYellow)
    {
        GetTopChip();
        Rigidbody2D rb = TopChip.GetComponent<Rigidbody2D>();
        ChipBehavior chipBehavior = TopChip.GetComponent<ChipBehavior>();
        chipBehavior.isMoving = false;
        if (isYellow)
        {
            rb.AddForce(Vector2.right * lateralSpeed);
        }
        else
        {
            chipBehavior.isMoving = false;
            rb.AddForce(Vector2.left * lateralSpeed);
        }
    }
}